import tensorflow as tf
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Flatten, Dense, Dropout, BatchNormalization, Conv2D, MaxPool2D, Activation, MaxPooling2D
from tensorflow.keras.optimizers import SGD
from tensorflow.keras.preprocessing import image
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from tqdm import tqdm
import keras

print("*** IMAGE PRE-PROCESSING")
csvData = pd.read_csv('metadata/chest_xray_metadata.csv')
csvData.shape

img_width = 350
img_height = 350

X = []
Y = []
for i in tqdm(range(csvData.shape[0])):
  path = 'metadata/'+ csvData['X_ray_image_name'][i]
  img = image.load_img(path, target_size=(img_width,img_height,3))
  img = image.img_to_array(img)
  img = img/255.0
  X.append(img)

X = np.array(X)
X.shape
y = csvData.drop(columns=['Unnamed: 0', 'X_ray_image_name', 'Label', 'Label_2_Virus_category'])[
    0:csvData.shape[0]]
y.head()
print(y.dtypes)

y = y.to_numpy()

for i in tqdm(range(csvData.shape[0])):
  if (pd.isnull(y[i])):
    label = [1, 0, 0]
  elif (y[i] == 'Virus'):
    label = [0, 1, 0]
  elif (y[i] == 'bacteria'):
    label = [0, 0, 1]
  Y.append(label)

#print(Y)
Y = np.array(Y)
Y.shape

X_train, X_test, y_train, y_test = train_test_split(X, Y, random_state =0, test_size=0.15)

#AlexNet
model = Sequential()

# 1st Convolutional Layer
model.add(Conv2D(filters=96, input_shape=(350,350,3), kernel_size=(11,11), strides=(4,4), padding='valid'))
model.add(Activation('relu'))
# Max Pooling
model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

# 2nd Convolutional Layer
model.add(Conv2D(filters=256, kernel_size=(11,11), strides=(1,1), padding='valid'))
model.add(Activation('relu'))
# Max Pooling
model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

# 3rd Convolutional Layer
model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
model.add(Activation('relu'))

# 4th Convolutional Layer
model.add(Conv2D(filters=384, kernel_size=(3,3), strides=(1,1), padding='valid'))
model.add(Activation('relu'))

# 5th Convolutional Layer
model.add(Conv2D(filters=256, kernel_size=(3,3), strides=(1,1), padding='valid'))
model.add(Activation('relu'))
# Max Pooling
model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2), padding='valid'))

# Passing it to a Fully Connected layer
model.add(Flatten())
# 1st Fully Connected Layer
model.add(Dense(4096, input_shape=(350*350*3,)))
model.add(Activation('relu'))
# Add Dropout to prevent overfitting
model.add(Dropout(0.1))

# 2nd Fully Connected Layer
model.add(Dense(4096))
model.add(Activation('relu'))
# Add Dropout
model.add(Dropout(0.2))

# 3rd Fully Connected Layer
model.add(Dense(1000))
model.add(Activation('relu'))
# Add Dropout
model.add(Dropout(0.3))

# Output Layer
model.add(Dense(3))
model.add(Activation('softmax'))

model.summary()

# Compile the model
opt = SGD(learning_rate=0.001)

model.compile(loss = "categorical_crossentropy", optimizer = opt, metrics=["accuracy"])
epochs = 5

from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import Callback
from tensorflow.keras.callbacks import ModelCheckpoint
class EarlyStoppingByLossVal(Callback):
    def __init__(self, monitor='val_accuracy', value=0.00001, verbose=0):
        super(Callback, self).__init__()
        self.monitor = monitor
        self.value = value
        self.verbose = verbose

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)
        if current > 0.84:
          print(current)
          if self.verbose > 0:
              print("Epoch %05d: early stopping THR" % epoch)
          self.model.stop_training = True


callback = EarlyStoppingByLossVal(monitor='val_accuracy', value=0.00001, verbose=1)
mcp_save = ModelCheckpoint('.mdl_wts.hdf5', save_best_only=True, monitor='val_accuracy')

history = model.fit(X_train, y_train, epochs=epochs, validation_data=(X_test, y_test),  callbacks = [mcp_save])

number_of_epochs_it_ran = len(history.history['loss'])

model.load_weights('.mdl_wts.hdf5')


test_loss, test_acc = model.evaluate(X_test,  y_test, verbose=2) # ovo je sa tensorflow sajta
print("Test loss: ", test_loss)
print("Test accuracy: ", test_acc)

print("*** VISUALIZING ACCURACY AND LOSS")
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']


loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)
plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('ACCURACY')
#plt.savefig('accuracy.png')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper left')
plt.title('LOSS')
#plt.savefig('Loss.png')
plt.show()

#TESTING
miss =0
hit = 0
test_data = pd.read_csv('metadata-test/chest_xray_test_dataset.csv')

Y_test_data = []
y_test_data = test_data.drop(columns=['Unnamed: 0', 'X_ray_image_name', 'Label', 'Label_2_Virus_category'])[0:test_data.shape[0]]
y_test_data = y_test_data.to_numpy()
for i in tqdm(range(624)):
  if (pd.isnull(y_test_data[i])):
    label = [1, 0, 0]
  elif (y_test_data[i] == 'Virus'):
    label = [0, 1, 0]
  elif (y_test_data[i] == 'bacteria'):
    label = [0, 0, 1]
  Y_test_data.append(label)

Y_test_data = np.array(Y_test_data)
print(y.shape)
Y_test_data.shape

#Test2
X_test_data = []
for i in tqdm(range(test_data.shape[0])):
  if (i == 624):
    break
  path = 'metadata-test/'+ str(test_data['X_ray_image_name'][i])
  klasa = test_data['Label_1_Virus_category'][i]
  img = image.load_img(path, target_size=(img_width,img_height,3))
  img = image.img_to_array(img)
  img = img/255.0
  X_test_data.append(img)
  img = img.reshape(1,img_width,img_height,3) # 3 ->rgb
  y_prob = model.predict(img) #vraca pretpostavku za svaku od targetovanih klasa

  if(y_prob[0][0] > y_prob[0][1] and y_prob[0][0]> y_prob[0][2]): #ako je normal
    if(pd.isnull(klasa)): #ako e klasa normal
      hit = hit +1
    else:
        miss = miss +1
  elif(y_prob[0][1] > y_prob[0][0] and y_prob[0][1]> y_prob[0][2]): #ako virus
    if(klasa == 'Virus'): #ako je klasa virus
      hit = hit +1
    else:
        miss = miss +1
  elif(y_prob[0][2] > y_prob[0][0] and y_prob[0][2]> y_prob[0][1]): #ako je bakterija
    if(klasa == 'bacteria'): #ako je klasa bakterija
      hit = hit +1
    else:
        miss = miss +1

X_test_data = np.array(X_test_data)
X_test_data.shape
X_train, X_test, y_train, y_test = train_test_split(X_test_data, Y_test_data, random_state =0, test_size=0.15)

test_loss, test_acc = model.evaluate(X_test,  y_test, verbose=2) # tensorflow sajt
print("Test loss: ", test_loss)
print("Test accuracy: ", test_acc)


print("HIT: " + str(hit) + "    MISS: " +  str(miss))