import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# preprocessing
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import warnings
warnings.filterwarnings(action="ignore")
# clustering
import sklearn
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture
from matplotlib import cm
from sklearn.metrics import silhouette_samples, silhouette_score

def main():
    csvData = pd.read_csv('credit_card_data.csv')
    print('Data shape: ' + str(csvData.shape))
    #print(csvData.isna().sum())

    #popunjvavanje null vrednosti
    csvData.loc[(csvData['MINIMUM_PAYMENTS'].isnull() == True), 'MINIMUM_PAYMENTS'] = csvData['MINIMUM_PAYMENTS'].median()
    csvData.loc[(csvData['CREDIT_LIMIT'].isnull() == True), 'CREDIT_LIMIT'] = csvData['CREDIT_LIMIT'].median()
    print(csvData.isna().sum()) # nakon popunjavanja

    csvData = csvData.drop('CUST_ID', 1)    #cust_id drop
    print('Data shape after CUST_ID: ' + str(csvData.shape))

    scaler = sklearn.preprocessing.StandardScaler()
    data_scaled = scaler.fit_transform(csvData)
    #data_scaled.shape

    data_imputed = pd.DataFrame(scaler.fit_transform(csvData.values), columns=csvData.columns)

    #Correlation Check
    plt.figure(figsize=(15, 15))
    sns.heatmap(data_imputed.corr(), annot=True, cmap='coolwarm',
                xticklabels=data_imputed.columns,
                yticklabels=data_imputed.columns)

    plt.savefig('Correlation.png')

    # Elbow method za odredjivanje optimalnog broja klastera

    nodes = []
    for x in range(1, 18):
        kmeans = KMeans(n_clusters=x,init='random', random_state=101)
        kmeans.fit(data_scaled)
        nodes.append(kmeans.inertia_)
    plt.figure(figsize=(12, 6))
    plt.plot(range(1, 18), nodes, marker='o')
    plt.xlabel('Number of Clusters')
    plt.ylabel('Inertia')
    plt.xticks(list(range(1, 18)))
    plt.savefig('Variance.png')

    kmeans = KMeans(n_clusters=6, random_state=101)

    # Redukcija na 6 komponenti
    new_data_pca = PCA(n_components=6).fit(data_scaled)
    new_data_trans = new_data_pca.fit_transform(data_imputed)
    data_scaled = pd.DataFrame(new_data_trans)

    # klasterovanje
    kmeans = kmeans.fit(data_scaled)

    print("group-Colors  ITEMS")
    print(pd.Series(kmeans.labels_).value_counts())

    colors_dict = {0: 'red', 1: 'green', 2: 'blue', 3: 'purple', 4: 'orange', 5: 'brown'}
    colors_list = []

    for i in kmeans.labels_:
        #print(i)
        colors_list.append(colors_dict[i])

    plt.figure(figsize=(5, 5))
    plt.scatter(new_data_trans[:, 0], new_data_trans[:, 1], color=colors_list, alpha=0.5)
    plt.savefig("Clusters2d.png")

    #sum
    sumarization = ['BALANCE', 'PURCHASES', 'ONEOFF_PURCHASES', 'INSTALLMENTS_PURCHASES',
                    'CASH_ADVANCE', 'PAYMENTS', 'MINIMUM_PAYMENTS']
    csvData = pd.DataFrame(data_imputed[sumarization])

    label = kmeans.fit_predict(csvData)
    csvData['group'] = label
    sumarization.append('group')

    sns.pairplot(csvData[sumarization], x_vars=sumarization[:-1], y_vars=['group'])
    plt.savefig('Sumarization.png')

# Eksplorativna analiza
    # select best columns
    best_cols = ["BALANCE", "PURCHASES", "CASH_ADVANCE", "CREDIT_LIMIT", "PAYMENTS", "MINIMUM_PAYMENTS"]

    # dataframe with best columns
    data_final = pd.DataFrame(data_imputed[best_cols])

    print('New dataframe with best columns has just been created. Data shape: ' + str(data_final.shape))

    alg = KMeans(n_clusters=6)
    label = alg.fit_predict(data_final)

    # create a 'cluster' column
    data_final['cluster'] = label
    best_cols.append('cluster')

    # make a Seaborn pairplot
    sns_plot = sns.pairplot(data_final[best_cols], hue='cluster')
    #sns.colors(colors_list)
    sns_plot.savefig('Plots.png')

    # sns.pairplot(data_final[best_cols], hue='cluster', x_vars=['PURCHASES', 'PAYMENTS', 'CREDIT_LIMIT'],
    #              y_vars=['cluster'],
    #              height=5, aspect=1)

    print('''
    group 5
    	- high purchuases
    	- third in payments
    	- low cash advance

    group 4
        - second highest in payments
    	- equal amount of onoff and installments purchuases 
    	- the most cash advance

    group 3
    	- highest payments
    	- huge onoff purchuases
    	- huge installments purchuases

    group 2
    	- third payments
    	- mostly installments purchuases
    	- equal amount of onoff and installments purchuases 

    group 1
    	- buying cheaper
    	- minmal payments vary
    	- fourth highest payments

    group 0
    	- lowest purchases
    	- second lowest payments
    	- equal amount of onoff and installments purchuases 
    ''')

if __name__ == '__main__':
    main()
