# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
#
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util

from game import Directions


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print("Start:", problem.getStartState())
    print("Is the start a goal?", problem.isGoalState(problem.getStartState()))
    print("Start's successors:", problem.getSuccessors(problem.getStartState()))
    """
    "*** YOUR CODE HERE ***"

    nodesStack = util.Stack()   # prazan stek cvorova
    #inicijalizacija sa pocetnim stanjem i stop komandom
    startNode = (problem.getStartState(), [Directions.STOP])
    nodesStack.push(startNode)

    visitedNodes = set()  #prazan set posecenih cvorova
    #depthSearchFirst
    while True:
        node = nodesStack.pop()
        nodeState = node[0]
        nodeDirections = node[1]

        #ako je dosao do goal state-a, vrati mi listu smerova od start-goal state-a
        if problem.isGoalState(nodeState):
            nodeDirections.pop(0)

            return nodeDirections

        elif nodeState not in visitedNodes: #poseti cvor ako vec nije
            visitedNodes.add(nodeState) #zapamti ovaj sto sam posetio

            for child in problem.getSuccessors(nodeState):  #smesti potomka u stek
                childState = child[0]
                childDirecton = child[1]

                newChildNode = (childState, nodeDirections+[childDirecton])
                nodesStack.push(newChildNode)
                #ako bih pushovao direktno u nodeDirection ne bi radilo
                #treba mi nova lista na koje cu dodavati cvorove potomaka
        else:
            """print "Node " + str(nodeState) + " already visited"""

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    "*** YOUR CODE HERE ***"
    startingNode = (problem.getStartState(), [Directions.STOP])
    myQueue = util.Queue()
    visitedNodes = set()    #poseceni cvorovi
    myQueue.push(startingNode)

    while True:
        currentNode = myQueue.pop()
        nodeState = currentNode[0]
        nodeDirection = currentNode[1]

        if problem.isGoalState(nodeState):
            nodeDirection.pop(0)

            return nodeDirection    #vraca mi listu directions-a od starta-kraja u koliko je dosao do goal state

        elif nodeState not in visitedNodes:
            visitedNodes.add(nodeState)

            for child in problem.getSuccessors(nodeState):
                childState = child[0]
                childDirection = child[1]

                newChildNode = (childState, nodeDirection + [childDirection])
                myQueue.push(newChildNode)

        else:
            """print "Node " + str(nodeState) + " already visited"""


def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"

    #lambda funkcija koja racuna svaki prelazak iz cvora u cvor pomocu funkc getCostOfAction()
    costOfActions = lambda path: problem.getCostOfActions(path)
    visitedNode = set() #ovde stavljam sve posecene cvorove

    myQueue = util.PriorityQueue()
    startingNodeState = problem.getStartState()
    startingNodeDirection = [Directions.STOP]
    startingNodeCost = costOfActions(startingNodeDirection) #treci element cvora
    startingNode = (startingNodeState, startingNodeDirection, startingNodeCost)

    myQueue.push(startingNodeState, startingNodeCost)

    while True:
        currentNode = myQueue.pop()
        nodeState = currentNode[0]
        nodeDirection = currentNode[1]

        if problem.isGoalState(nodeState):
            nodeDirection.pop(0)
            return nodeDirection
        elif nodeState not in visitedNode:
            visitedNode.add(nodeState)

            for child in problem.getSuccessors(nodeState):
                childState = child[0]

                childDirection = nodeDirection + [child[1]]
                childNodeCost = costOfActions(childDirection)

                newChildNode = (childState, childDirection + childNodeCost)
                myQueue.push(newChildNode, childNodeCost)
        else:
            """print "Node " + str(nodeState) + " already visited"""


def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    "*** YOUR CODE HERE ***"
    #za razliku od ucs-a a* ima skor = cost of path + heuristic
    costOfActions = lambda state, path: problem.getCostOfActions(path) + heuristic(state, problem)
    visitedNodes = set()
    myQueue = util.PriorityQueue()

    startingNodeState = problem.getStartState()
    startingNodeDirection = [Directions.STOP]
    startingNodeCost = costOfActions(startingNodeState, startingNodeDirection) #vrednost putanje izmedju 2 cvora

    startingNode = (startingNodeState, startingNodeDirection,startingNodeCost)

    myQueue.push(startingNode, startingNodeCost)

    while True:
        currentNode = myQueue.pop()
        nodeState = currentNode[0]
        nodeDirection = currentNode[1]

        if problem.isGoalState(nodeState):
            nodeDirection.pop(0)    #skida mi iz reda Directions.STOP
            return nodeDirection

        elif nodeState not in visitedNodes:
            visitedNodes.add(nodeState) #sacuvaj mi cvor kao posecen

            for child in problem.getSuccessors(nodeState):
                childState = child[0]

                childDirection = nodeDirection + [child[1]]
                childNodeCost = costOfActions(childState, childDirection)

                newChildNode = (childState, childDirection + childNodeCost)
                myQueue.push(newChildNode, childNodeCost)

        else:
            """print "Node " + str(nodeState) + " already visited"""

def estimateFoodHeuristic(state, problem):

    listOfFood = problem.food.asList()

    if len(listOfFood == 0):
        return 0
    x,y = state #koordinate od pacmana

    foodDistance = []

    for x1,y1 in listOfFood:
        foodDistance.append(util.manhattanDistance((x, y), (x1, y1)))

    firstFoodToGrab = min(foodDistance)   #distanca do najblize hrane
    foodDistance.remove(firstFoodToGrab)

    try:
        averageFoodDistance = sum(foodDistance) / len(foodDistance)
    except:
        averageFoodDistance = 0

    return firstFoodToGrab + averageFoodDistance

# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
